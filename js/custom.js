// graphs
var color = Chart.helpers.color;
var barChartData = {
    labels: ["FY 16", "FY 17", "FY 18"],

    datasets: [{
        label: 'Revenue',
        backgroundColor: '#0b7dbb',
        borderColor: '#0b7dbb',
        borderWidth: 1,
        data: [
        350, 300, 380
        ]
    }, {
        label: '(in Cr)PAT',
        backgroundColor: '#2eba9d',
        borderColor: '#2eba9d',
        borderWidth: 1,
        data: [
        200, 250, 80
        ]
    }]
};

var barChartData2 = {
    labels: ["FY 18"],
    datasets: [{
        label: 'Debt / Equity Company',
        backgroundColor: '#0b7dbb',
        borderColor: '#0b7dbb',
        borderWidth: 1,
        data: [
        3, 2, 1
        ]
    }, {
        label: 'Industry Standard',
        backgroundColor: '#2eba9d',
        borderColor: '#2eba9d',
        borderWidth: 1,
        data: [
        2.5, 2, 3
        ]
    }]
};

window.onload = function() {

    var ctx = document.getElementById("chart-one").getContext("2d");
    window.myBar = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            responsive: true,
            legend: {
                position: 'bottom',
            },
            title: {
                display: true,
                text: ''
            }
        }
    });

    var ctx2 = document.getElementById("chart-two").getContext("2d");
    window.myBar2 = new Chart(ctx2, {
        type: 'bar',
        data: barChartData2,
        options: {
            responsive: true,
            legend: {
                position: 'bottom',
            },
            title: {
                display: true,
                text: ''
            }
        }
    });
};
// $(document).ready(function(){
//     $('.tabs .tab-links a').on('click', function(e)  {
//         var currentAttrValue = $(this).attr('href');

//         // Show/Hide Tabs
//         $('.tabs ' + currentAttrValue).show().siblings().hide();

//         // Change/remove current tab to active
//         $(this).parent('li').addClass('active').siblings().removeClass('active');

//         e.preventDefault();
//     });
// });
// end

// $(function() {
//  $('.Pricex').click(function(){
//    $("#bg").attr('src',"assets/pricex.svg");
//  });
// });
// navbar
$(document).ready(function() {
    // $("#edit_user_details").click(function(e){
    //     var min = [10,20,30];
    //     var newHTML = [];
    //     for (var i = 0; i < min.length; i++) {
    //         newHTML.push('<li><a class="minprice">' + min[i] + " " +"L" +'</a></li>');
    //     }
    //         // var focEl=$(this).parent('.dropdown').find('ul.dropdown-menu #min-foc');
    //         $(".min-price").html(newHTML.join(""));
    //         $('#min-foc').focus();            
    //     });
    // $(".minprice").on('click',function(){
    //     alert();
    // });
    
    $('#services-dropdown').on('click', function() {
        $('.services-dropdown').addClass('is-active-dropdown');
    });
    $('.dd-close').on('click', function() {
        $('.services-dropdown').removeClass('is-active-dropdown');
    });
    $('#products-dropdown').on('click', function() {
        $('.products-dropdown').addClass('is-active-dropdown');
    });
    $('.dd-close').on('click', function() {
        $('.products-dropdown').removeClass('is-active-dropdown');
    });
});
$("#budgetDropdown").on('click', '#edit_user_details', function(event) {
        event.preventDefault();
        /* Act on the event */
        console.log(clicked);
        console.log($(this));
        var amt = [10,20,30];
        var minamt='';
        var maxamt='';
        minamt+='<ul id="minamtRange">';
        maxamt+='<ul id="maxamtRange">';
        for (var i = 0; i < amt.length; i++) {
            console.log(amt[i]);
            minamt+='<li><a class="minprice">' + amt[i] + " " +"L" +'</a></li>'
            maxamt+='<li><a class="maxprice">' + amt[i] + " " +"L" +'</a></li>'
        }

        minamt+='</ul>';
        maxamt+='</ul>';
        $(".amtRangePicker").html(minamt);
    });


