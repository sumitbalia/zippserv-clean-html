function gethospital() {
        var position = { lat: 12.915680, lng: 77.650929 };
        var marker;
        var placesList = document.getElementById('places');
        var map = new google.maps.Map(document.getElementById('inner-map'), {
            center: position,
            zoom: 15,
            styles: [{
                    "featureType": "all",
                    "stylers": [{
                            "saturation": 0
                        },
                        {
                            "hue": "#e7ecf0"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "stylers": [{
                        "saturation": -70
                    }]
                },
                {
                    "featureType": "transit",
                    "stylers": [{
                        "visibility": "off"
                    }]
                },
                {
                    "featureType": "poi",
                    "stylers": [{
                        "visibility": "off"
                    }]
                },
                {
                    "featureType": "water",
                    "stylers": [{
                            "visibility": "simplified"
                        },
                        {
                            "saturation": -60
                        }
                    ]
                }
            ]
        });
        var service = new google.maps.places.PlacesService(map);
        service.nearbySearch({
            location: position,
            radius: 500,
            type: ['hospital']
        }, processResults);

        function processResults(results, status, pagination) {
            if (status !== google.maps.places.PlacesServiceStatus.OK) {
                return;
            } else {
                createMarkers(results);
            }
        }

        function createMarkers(places) {
            var bounds = new google.maps.LatLngBounds();
            var placesList = document.getElementById('places');

            for (var i = 0, place; place = places[i]; i++) {
                var image = {
                    url: place.icon,
                    scaledSize: new google.maps.Size(25, 25)
                };

                var marker = new google.maps.Marker({
                    map: map,
                    icon: image,
                    title: place.name,
                    position: place.geometry.location
                });
                // placesList.innerHTML += '<li>' + place.name + '</li>';
                bounds.extend(place.geometry.location);
                // var geocoder = new google.maps.Geocoder;
                // geocoder.geocode({'location': position}, function(results, status) {
                // if (status === 'OK') {
                //     for (var i = 0, place; place = places[i]; i++){
                //       console.log(results[i].formatted_address);
                //       var address = results[i].formatted_address;
                //     }

                //   }
                // });

                var infowindow = new google.maps.InfoWindow();
                var content = '<div class="map-info"><h4 class="m0 clr-blue">' + place.name + '</h4><hr>' + '<p>' + place.geometry.location + '</p></div>';
                google.maps.event.addListener(marker, 'mouseover', (function(marker, content, infowindow) {
                    return function() {
                        infowindow.setContent(content);
                        infowindow.open(map, marker);
                    };
                })(marker, content, infowindow));
            }
            map.fitBounds(bounds);
        }
    }

    function getschool() {
        var position = { lat: 12.915680, lng: 77.650929 };
        var marker;
        var placesList = document.getElementById('places');
        var map = new google.maps.Map(document.getElementById('inner-map'), {
            center: position,
            zoom: 15,
            styles: [{
                    "featureType": "all",
                    "stylers": [{
                            "saturation": 0
                        },
                        {
                            "hue": "#e7ecf0"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "stylers": [{
                        "saturation": -70
                    }]
                },
                {
                    "featureType": "transit",
                    "stylers": [{
                        "visibility": "off"
                    }]
                },
                {
                    "featureType": "poi",
                    "stylers": [{
                        "visibility": "off"
                    }]
                },
                {
                    "featureType": "water",
                    "stylers": [{
                            "visibility": "simplified"
                        },
                        {
                            "saturation": -60
                        }
                    ]
                }
            ]
        });
        var service = new google.maps.places.PlacesService(map);
        service.nearbySearch({
            location: position,
            radius: 500,
            type: ['school']
        }, processResults);

        function processResults(results, status, pagination) {
            if (status !== google.maps.places.PlacesServiceStatus.OK) {
                return;
            } else {
                createMarkers(results);
            }
        }

        function createMarkers(places) {
            var bounds = new google.maps.LatLngBounds();
            var placesList = document.getElementById('places');

            for (var i = 0, place; place = places[i]; i++) {
                var image = {
                    url: place.icon,
                    scaledSize: new google.maps.Size(25, 25)
                };

                var marker = new google.maps.Marker({
                    map: map,
                    icon: image,
                    title: place.name,
                    position: place.geometry.location
                });

                var infowindow = new google.maps.InfoWindow();
                var content = '<h4 class="m0 clr-blue">' + place.name + '</h4><hr>' + '<p>' + place.geometry.location + '</p>';
                google.maps.event.addListener(marker, 'mouseover', (function(marker, content, infowindow) {
                    return function() {
                        infowindow.setContent(content);
                        infowindow.open(map, marker);
                    };
                })(marker, content, infowindow));

                // placesList.innerHTML += '<li>' + place.name + '</li>';
                bounds.extend(place.geometry.location);
            }
            map.fitBounds(bounds);
        }
    }

    function getentertainment() {
        var position = { lat: 12.915680, lng: 77.650929 };
        var marker;
        var map = new google.maps.Map(document.getElementById('inner-map'), {
            center: position,
            zoom: 15,
            styles: [{
                    "featureType": "all",
                    "stylers": [{
                            "saturation": 0
                        },
                        {
                            "hue": "#e7ecf0"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "stylers": [{
                        "saturation": -70
                    }]
                },
                {
                    "featureType": "transit",
                    "stylers": [{
                        "visibility": "off"
                    }]
                },
                {
                    "featureType": "poi",
                    "stylers": [{
                        "visibility": "off"
                    }]
                },
                {
                    "featureType": "water",
                    "stylers": [{
                            "visibility": "simplified"
                        },
                        {
                            "saturation": -60
                        }
                    ]
                }
            ]
        });
        var infowindow = new google.maps.InfoWindow();

        var service = new google.maps.places.PlacesService(map);
        service.nearbySearch({
            location: position,
            radius: 500,
            types: ['movie_theater', 'night_club', 'zoo']
        }, processResults);

        function processResults(results, status, pagination) {
            if (status !== google.maps.places.PlacesServiceStatus.OK) {
                return;
            } else {
                createMarkers(results);
            }
        }

        function createMarkers(places) {
            var bounds = new google.maps.LatLngBounds();
            var placesList = document.getElementById('places');

            for (var i = 0, place; place = places[i]; i++) {
                var image = {
                    url: place.icon,
                    scaledSize: new google.maps.Size(25, 25)
                };

                var marker = new google.maps.Marker({
                    map: map,
                    icon: image,
                    title: place.name,
                    position: place.geometry.location
                });

                var infowindow = new google.maps.InfoWindow();
                var content = '<h4 class="m0 clr-blue">' + place.name + '</h4><hr>' + '<p>' + place.geometry.location + '</p>';
                google.maps.event.addListener(marker, 'mouseover', (function(marker, content, infowindow) {
                    return function() {
                        infowindow.setContent(content);
                        infowindow.open(map, marker);
                    };
                })(marker, content, infowindow));

                bounds.extend(place.geometry.location);
            }
            map.fitBounds(bounds);
        }
    }

    function getbusstop() {
        var position = { lat: 12.915680, lng: 77.650929 };
        var marker;
        var map = new google.maps.Map(document.getElementById('inner-map'), {
            center: position,
            zoom: 15,
            styles: [{
                    "featureType": "all",
                    "stylers": [{
                            "saturation": 0
                        },
                        {
                            "hue": "#e7ecf0"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "stylers": [{
                        "saturation": -70
                    }]
                },
                {
                    "featureType": "transit",
                    "stylers": [{
                        "visibility": "off"
                    }]
                },
                {
                    "featureType": "poi",
                    "stylers": [{
                        "visibility": "off"
                    }]
                },
                {
                    "featureType": "water",
                    "stylers": [{
                            "visibility": "simplified"
                        },
                        {
                            "saturation": -60
                        }
                    ]
                }
            ]
        });
        var service = new google.maps.places.PlacesService(map);
        service.nearbySearch({
            location: position,
            radius: 500,
            type: ['bus_station']
        }, processResults);

        function processResults(results, status, pagination) {
            if (status !== google.maps.places.PlacesServiceStatus.OK) {
                return;
            } else {
                createMarkers(results);
            }
        }

        function createMarkers(places) {
            var bounds = new google.maps.LatLngBounds();
            var placesList = document.getElementById('places');

            for (var i = 0, place; place = places[i]; i++) {
                var image = {
                    url: place.icon,
                    scaledSize: new google.maps.Size(25, 25)
                };

                var marker = new google.maps.Marker({
                    map: map,
                    icon: image,
                    title: place.name,
                    position: place.geometry.location
                });

                var infowindow = new google.maps.InfoWindow();
                var content = '<h4 class="m0 clr-blue">' + place.name + '</h4><hr>' + '<p>' + place.geometry.location + '</p>';
                google.maps.event.addListener(marker, 'mouseover', (function(marker, content, infowindow) {
                    return function() {
                        infowindow.setContent(content);
                        infowindow.open(map, marker);
                    };
                })(marker, content, infowindow));

                bounds.extend(place.geometry.location);
            }
            map.fitBounds(bounds);
        }
    }

    function getgrocery() {
        var position = { lat: 12.915680, lng: 77.650929 };
        var marker;
        var map = new google.maps.Map(document.getElementById('inner-map'), {
            center: position,
            zoom: 15,
            styles: [{
                    "featureType": "all",
                    "stylers": [{
                            "saturation": 0
                        },
                        {
                            "hue": "#e7ecf0"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "stylers": [{
                        "saturation": -70
                    }]
                },
                {
                    "featureType": "transit",
                    "stylers": [{
                        "visibility": "off"
                    }]
                },
                {
                    "featureType": "poi",
                    "stylers": [{
                        "visibility": "off"
                    }]
                },
                {
                    "featureType": "water",
                    "stylers": [{
                            "visibility": "simplified"
                        },
                        {
                            "saturation": -60
                        }
                    ]
                }
            ]
        });
        var service = new google.maps.places.PlacesService(map);
        service.nearbySearch({
            location: position,
            radius: 500,
            type: ['grocery_or_supermarket']
        }, processResults);

        function processResults(results, status, pagination) {
            if (status !== google.maps.places.PlacesServiceStatus.OK) {
                return;
            } else {
                createMarkers(results);
            }
        }

        function createMarkers(places) {
            var bounds = new google.maps.LatLngBounds();
            var placesList = document.getElementById('places');

            for (var i = 0, place; place = places[i]; i++) {
                var image = {
                    url: place.icon,
                    scaledSize: new google.maps.Size(25, 25)
                };

                var marker = new google.maps.Marker({
                    map: map,
                    icon: image,
                    title: place.name,
                    position: place.geometry.location
                });

                var infowindow = new google.maps.InfoWindow();
                var content = '<h4 class="m0 clr-blue">' + place.name + '</h4><hr>' + '<p>' + place.geometry.location + '</p>';
                google.maps.event.addListener(marker, 'mouseover', (function(marker, content, infowindow) {
                    return function() {
                        infowindow.setContent(content);
                        infowindow.open(map, marker);
                    };
                })(marker, content, infowindow));

                bounds.extend(place.geometry.location);
            }
            map.fitBounds(bounds);
        }
    }